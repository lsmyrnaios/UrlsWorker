# This script adds swap in the System.

# Info found in: https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-20-04


# Creating a Swap File with 6 Gb
sudo fallocate -l 6G /swapfile
ls -lh /swapfile

# Enabling the Swap File
sudo chmod 600 /swapfile
ls -lh /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
free -h

# Making the Swap File Permanent
sudo cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

# Adjusting the Swappiness Property
cat /proc/sys/vm/swappiness
sudo sysctl vm.swappiness=10  # Setting it closer to 0, makes the swaps more rare: 10%. The default is 60%.
echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.conf  # Append it to that file in order for the custom "swappiness" to be available on restart.
