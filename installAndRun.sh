# This script installs and runs the project.
# It also sets the "max-heap-size", depending on the machine's memory.

# For error-handling, we cannot use the "set -e" since: it has problems https://mywiki.wooledge.org/BashFAQ/105
# So we have our own function, for use when a single command fails.
handle_error () {
  echo -e "$1"; exit $2
}

# Change the working directory to the script's directory, when running from another location.
cd "${0%/*}" || handle_error "Could not change-dir to this script's dir!" 1

justRun=0

if [[ $# -eq 1 ]]; then
	justRun=$1
elif [[ $# -gt 1 ]]; then
	echo -e "Wrong number of arguments given: ${#} (more than 1)\nPlease execute it like: script.sh <justRun: 0 | 1>";	exit 2
fi

gradleVersion="8.12.1"

shouldBeCarefulWithMaxHeap=0 # This is NOT a cmd-arg.

if [[ justRun -eq 0 ]]; then

  # Install the specified Gradle-build-tool version, if it does not exist.
  if [[ ! -d /opt/gradle/gradle-${gradleVersion} ]]; then
    wget https://services.gradle.org/distributions/gradle-${gradleVersion}-bin.zip
    echo -e "\nAsking for sudo, in order to install 'gradle'..\n"
    sudo mkdir /opt/gradle
    sudo apt install -y unzip && sudo unzip -d /opt/gradle gradle-${gradleVersion}-bin.zip
    sudo rm -rf gradle-${gradleVersion}-bin.zip
    #ls /opt/gradle/gradle-${gradleVersion}  # For debugging installation
  fi

  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH

  if [[ shouldBeCarefulWithMaxHeap -eq 1 ]]; then
    # Update the max-heap-size based on the machine's physical memory.
    machine_memory_mb=$(grep MemTotal /proc/meminfo | awk '{print $2}' | xargs -I {} echo "scale=4; {}/1024" | bc)  # It returns the size in MB.
    max_heap_size_mb=$(echo "($machine_memory_mb - 800)/1" | bc) # Leave 800 MB to the system (the "()/1" is used to take the floor value).
    # Now, we replace the "-Xmx" parameter inside the "./build.gradle" file, with "-Xmx${max_heap_size}m"
    echo -e "\n\nThe max-heap-size (-Xmx) will be set to: ${max_heap_size_mb}m\n\n"
    sed -i "s/'-Xmx[0-9]\+[gm]'/'-Xmx${max_heap_size_mb}m'/g" ./build.gradle
  fi

  gradle wrapper --gradle-version=${gradleVersion} --distribution-type=bin

  #gradle tasks  # For debugging installation
  #gradle -v # For debugging installation

  gradle clean build --refresh-dependencies # --info
else
  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH # Make sure the gradle is still accessible (it usually isn't without the "export").
fi

gradle bootRun
