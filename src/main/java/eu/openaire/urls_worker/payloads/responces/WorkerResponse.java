package eu.openaire.urls_worker.payloads.responces;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "workerId",
        "assignmentsLimit"
})
public class WorkerResponse {

    @JsonProperty("workerId")
    private String workerId;

    @JsonProperty("assignmentsLimit")
    private int assignmentsLimit;

    public WorkerResponse(String workerId, int assignmentsLimit) {
        this.workerId = workerId;
        this.assignmentsLimit = assignmentsLimit;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public int getTasksLimit() {
        return assignmentsLimit;
    }

    public void setTasksLimit(int assignmentsLimit) {
        this.assignmentsLimit = assignmentsLimit;
    }

    @Override
    public String toString() {
        return "WorkerRequest{" +
                "id='" + workerId + '\'' +
                ", assignmentsLimit=" + assignmentsLimit +
                '}';
    }
}
