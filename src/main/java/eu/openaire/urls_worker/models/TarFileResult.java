package eu.openaire.urls_worker.models;

import java.io.File;

public class TarFileResult {

	private File tarFile;
	private int numTarredFiles;

	public TarFileResult(File tarFile, int numTarredFiles) {
		this.tarFile = tarFile;
		this.numTarredFiles = numTarredFiles;
	}

	public File getTarFile() {
		return tarFile;
	}

	public int getNumTarredFiles() {
		return numTarredFiles;
	}
}
