package eu.openaire.urls_worker;

import eu.openaire.publications_retriever.PublicationsRetriever;
import eu.openaire.urls_worker.components.ConnWithController;
import eu.openaire.urls_worker.controllers.FullTextsController;
import eu.openaire.urls_worker.util.UriBuilder;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.TimeUnit;


@SpringBootApplication
@EnableConfigurationProperties
@EnableScheduling
public class UrlsWorkerApplication {

    private static final Logger logger = LoggerFactory.getLogger(UrlsWorkerApplication.class);

    private static String workerId;

    private static ConfigurableApplicationContext context;

    @Autowired
    private ConnWithController connWithController;


    public UrlsWorkerApplication(@Value("${services.pdf_aggregation.worker.workerId}") String workerId)
    {
        UrlsWorkerApplication.workerId = workerId;
    }


    public static void main(String[] args) {
        context = SpringApplication.run(UrlsWorkerApplication.class, args);

        Runtime javaRuntime = Runtime.getRuntime();
        int mb = 1048576;
        logger.debug("HeapSize: " + (javaRuntime.totalMemory() / mb) + " mb");
        logger.debug("HeapMaxSize: " + (javaRuntime.maxMemory() / mb) + " mb");
        logger.debug("HeapFreeSize: " + (javaRuntime.freeMemory() / mb) + " mb");

        logger.info("The Worker has started running. Its id is: \"" + workerId + "\".");
    }


    public static void gentleAppShutdown()
    {
        int exitCode = 0;
        try {
            exitCode = SpringApplication.exit(context, () -> 0);    // The "PreDestroy" method will be called. (the "context" will be closed automatically (I checked it))
        } catch (IllegalArgumentException iae) {
            logger.error(iae.getMessage()); // This will say "Context must not be null", in case the "gentleAppShutdown()" was called too early in the app's lifetime. But it's ok.
        }
        System.exit(exitCode);
    }


    @PreDestroy
    public void preDestroy()
    {
        if ( PublicationsRetriever.executor != null )
        {
            logger.info("Shutting down the threads used by \"PublicationsRetriever\"-plugin..");
            PublicationsRetriever.executor.shutdown();	// Define that no new tasks will be scheduled.
            try {
                if ( !PublicationsRetriever.executor.awaitTermination(2, TimeUnit.MINUTES) ) {
                    logger.warn("The working threads did not finish on time! Stopping them immediately..");
                    PublicationsRetriever.executor.shutdownNow();
                }
            } catch (SecurityException se) {
                logger.error("Could not shutdown the threads in any way..!", se);
            } catch (InterruptedException ie) {
                try {
                    PublicationsRetriever.executor.shutdownNow();
                } catch (SecurityException se) {
                    logger.error("Could not shutdown the threads in any way..!", se);
                }
            }
        }

        FullTextsController.deleteAssignmentsDirectory(-1, null);

        // Inform the Controller that this worker will shut down.
        connWithController.postShutdownReportToController(workerId);

        logger.info("Exiting..");
    }


    @Bean
    public CommandLineRunner setServerBaseUrl(Environment environment, ServletWebServerApplicationContext webServerAppCtxt)
    {
        return args -> new UriBuilder(environment, webServerAppCtxt);
    }

}
