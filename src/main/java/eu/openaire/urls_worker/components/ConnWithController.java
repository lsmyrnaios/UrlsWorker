package eu.openaire.urls_worker.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


@Component
public class ConnWithController {

    private static final Logger logger = LoggerFactory.getLogger(ConnWithController.class);

    private final String controllerBaseUrl;


    public ConnWithController(@Value("${services.pdf_aggregation.worker.controllerBaseUrl}") String controllerBaseUrl) {
        this.controllerBaseUrl = controllerBaseUrl;
    }


    public boolean postShutdownReportToController(String workerId)
    {
        logger.info("Going to \"postShutdownReportToController\".");
        String url = this.controllerBaseUrl + "workerShutdownReport?workerId=" + workerId;
        try {
            ResponseEntity<String> responseEntity = new RestTemplate().postForEntity(url, null, String.class);
            int responseCode = responseEntity.getStatusCodeValue();
            if ( responseCode != HttpStatus.OK.value() ) {
                logger.error("HTTP-Connection problem with the submission of the \"postShutdownReportToController\"! Error-code was: " + responseCode);
                return false;
            }
        } catch (HttpServerErrorException hsee) {
            logger.error("The Controller failed to handle the \"postShutdownReportToController\": " + hsee.getMessage());
            return false;
        } catch (HttpClientErrorException hcee) {
            logger.error("The Worker did something wrong when sending the report result to the Controller. | url: " + url + "\n" + hcee.getMessage());
            return false;
        } catch (Exception e) {
            String errorMsg = "Error for \"postShutdownReportToController\", to the Controller.";
            Throwable cause = e.getCause();
            String exMsg;
            if ( (cause != null) && ((exMsg = cause.getMessage()) != null) && exMsg.contains("Connection refused") )
                logger.error(errorMsg + " | The Controller has probably crashed, since we received a \"Connection refused\" message!");
            else
                logger.error(errorMsg, e);
            return false;
        }
        return true;
    }

}
