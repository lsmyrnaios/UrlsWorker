package eu.openaire.urls_worker.services;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


@Service
public class FileStorageService {

    private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    public String assignmentsBaseLocation = null;


    @Autowired
    public FileStorageService(@Value("${services.pdf_aggregation.worker.assignmentsDir}") String assignmentsBaseLocation) {
        this.assignmentsBaseLocation = assignmentsBaseLocation;

        // In case the user-defined storageDir starts with "./", then replace that part with the actual user.dir", in order to have valid storage-locations for fileName-extraction in the Controller, even if the files are correctly downloaded there.
        if ( this.assignmentsBaseLocation.startsWith("." + File.separator) )
            this.assignmentsBaseLocation = ((System.getProperty("user.dir") + File.separator) + StringUtils.replace(this.assignmentsBaseLocation, ("." + File.separator), "", 1));

        if ( !this.assignmentsBaseLocation.endsWith(File.separator) )
            this.assignmentsBaseLocation += File.separator;

        // Create the base-directory.
        try {
            Files.createDirectories(Paths.get(this.assignmentsBaseLocation));
        } catch (Exception e) {
            logger.error("Could not create the base-directory where the downloaded files will be stored!", e);
            System.exit(-10);
        }
    }

}
